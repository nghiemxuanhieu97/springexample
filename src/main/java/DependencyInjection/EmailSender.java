package DependencyInjection;

import org.springframework.stereotype.Component;

@Component
public class EmailSender implements Sender {
    @Override
    public void send(String email) {
        //Handling
        System.out.println("Confimation was sent successfully to email: "+email);
    }
    //This class has module: send confirmation to customer
}
