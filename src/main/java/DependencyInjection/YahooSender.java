package DependencyInjection;

import org.springframework.stereotype.Component;

@Component
public class YahooSender implements Sender {
    @Override
    public void send(String yahoo) {
        //Handling
        System.out.println("Confimation was sent successfully to yahoo: "+yahoo);
    }
}
