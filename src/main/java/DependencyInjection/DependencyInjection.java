package DependencyInjection;

public class DependencyInjection {


    public static void main(String[] args) {
        //This Webapp will get info that customer enter and use method reservationCheckout of class CustomerBooking to solve
        //If I want to change to YahooSender. I just change EmailSender to YahooSender and I don't change anything in class CustomerBooking
        //We see high class: CustomerBooking not depend on low class: EmailSender or YahooSender. They depend on Interface Sender.
        //LooseCoupling is a part of Dependency Inversion (One of principles of SOLID)

        int IDCustomer = 5;
        String roomName = "Single Room";
        String email = "nghiemxuanhieu97@gmail.com";
        String yahoo = "nghiemxuanhieu97@yahoo.com";
        //Use Constructure Injection
        CustomerBooking customerBooking = new CustomerBooking(new CustomerInformation(), new YahooSender(),new CancellationRequirement());
        customerBooking.reservationCheckout(IDCustomer,roomName,yahoo);
        //Use Setter Injection
        CustomerBooking customerBooking1 = new CustomerBooking();
        customerBooking1.setInformation(new CustomerInformation());
        customerBooking1.setSender(new EmailSender());
        customerBooking1.setRequirement(new CancellationRequirement());
        customerBooking1.reservationCheckout(IDCustomer,roomName,email);




    }

}

