package DependencyInjection;

public interface Requirement {
    void request(int idCustomer, String nameRoom);
}
