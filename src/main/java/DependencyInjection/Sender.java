package DependencyInjection;

public interface Sender {
    void send(String mail);
}
