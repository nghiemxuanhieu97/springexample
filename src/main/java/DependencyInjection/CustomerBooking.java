package DependencyInjection;

public class CustomerBooking {
    Information information;
    Sender sender;
    Requirement requirement;
    CustomerBooking(){}
    //Constructure Injection
    CustomerBooking(Information info, Sender sen, Requirement req){
        this.information = info;
        this.sender = sen;
        this.requirement = req;
    }
    //Properties Injection
    public void setInformation(Information information) {
        this.information = information;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public void setRequirement(Requirement requirement) {
        this.requirement = requirement;
    }
    //Method check out reservation
    void reservationCheckout(int ID, String name, String mailbox){
        information.getInfo(ID,name,mailbox);
        sender.send(mailbox);
        requirement.request(ID,name);
    }

}
