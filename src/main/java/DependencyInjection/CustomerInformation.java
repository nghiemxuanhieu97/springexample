package DependencyInjection;

public class CustomerInformation implements Information {
    int ID;
    String nameRoom;
    String email;

    @Override
    public void getInfo(int ID, String nameRoom, String mailbox) {
        this.ID = ID;
        this.nameRoom = nameRoom;
        this.email = mailbox;
        System.out.println("Info: ID = "+this.ID+", nameRoom = "+this.nameRoom+", mailBox = "+this.email);
    }
    //This class has module: get information of customer

}
