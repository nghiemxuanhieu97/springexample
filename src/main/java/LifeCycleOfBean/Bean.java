package LifeCycleOfBean;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class Bean {
    @PostConstruct
    void HangdleAfterInitialization(){
        System.out.println("I use @PostConstruct");

    }
    @PreDestroy
    void HandleBeforeDestruction(){
        System.out.println("I use @PreDestroy");
    }
}
