package LifeCycleOfBean;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Life {
    public static void main(String[] args) {
        System.out.println("Before I create DI container");
        ApplicationContext context = SpringApplication.run(Life.class);
        System.out.println("After I create DI container");

        Bean bean = context.getBean(Bean.class);
        System.out.println("I created Bean");
        System.out.println("Before I destroy Bean");
        ((ConfigurableApplicationContext) context).getBeanFactory().destroyBean(bean);
        System.out.println("After I destroyed Bean");
    }
}
