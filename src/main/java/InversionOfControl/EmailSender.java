package InversionOfControl;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component("email")
@Primary
public class EmailSender implements Sender {
    @Override
    public void send(String email) {
        //Handling
        System.out.println("Confimation was sent successfully to email: "+email);
    }
    //This class has module: send confirmation to customer
}
