package InversionOfControl;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class IoC {
    public static void main(String[] args) {
        int IDCustomer = 5;
        String roomName = "Single Room";
        String email = "nghiemxuanhieu97@gmail.com";
        String yahoo = "nghiemxuanhieu97@yahoo.com";
        ApplicationContext context = SpringApplication.run(IoC.class,args);

//        Information info = context.getBean(Information.class);
//        Requirement requirement = context.getBean(Requirement.class);
//        Sender sender = context.getBean(Sender.class);
//        CustomerBooking customerBooking = new CustomerBooking(info,sender,requirement);
//        ---------------------------------------------------------------------------------------------------
//        In case of taking dependencies of interfaces, I annonate @Component to class CustomerBooking,
//        @Autowired: SpringBoot auto inject one instance when CustomerBooking is created.
        CustomerBooking customerBooking = context.getBean(CustomerBooking.class);
        customerBooking.reservationCheckout(IDCustomer,roomName,email);
    }

    
}

