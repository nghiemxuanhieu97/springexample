package InversionOfControl;

public interface Requirement {
    void request(int idCustomer, String nameRoom);
}
