package InversionOfControl;

public interface Sender {
    void send(String mail);
}
