package Coupling.LooseCoupling;

public interface Information {
    void getInfo(int ID, String nameRoom, String mailbox);

}
