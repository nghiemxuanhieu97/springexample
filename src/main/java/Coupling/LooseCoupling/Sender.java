package Coupling.LooseCoupling;

public interface Sender {
    void send(String mail);
}
