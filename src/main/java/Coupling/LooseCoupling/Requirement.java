package Coupling.LooseCoupling;

public interface Requirement {
    void request(int idCustomer, String nameRoom);
}
