package Coupling.LooseCoupling;

public class CustomerBooking {
    Information information;
    Sender sender;
    Requirement requirement;
    //Constructor Injection
    CustomerBooking(Information info, Sender sen, Requirement req){
        this.information = info;
        this.sender = sen;
        this.requirement = req;

    }

    void reservationCheckout(int ID, String name, String mailbox){
        information.getInfo(ID,name,mailbox);
        sender.send(mailbox);
        requirement.request(ID,name);
    }

}
