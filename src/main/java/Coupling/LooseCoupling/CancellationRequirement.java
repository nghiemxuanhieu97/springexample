package Coupling.LooseCoupling;

public class CancellationRequirement implements Requirement {
    //This class has module: solve request cancellation of Customer
    @Override
    public void request(int idCustomer, String nameRoom){
        System.out.println("Customer with ID: "+idCustomer+" cancelled "+nameRoom);
    }
}
