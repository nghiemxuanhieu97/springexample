package Coupling.TightCoupling;

public class CustomerBooking {
    //Assume:
    //If I want to send to notification to Customer's yahoo but class "Sender" just send confirmation to email of customer
    // --> Rewrite class Sender
    //If I want to get Information of someone except customer
    // --> Write new class and it will affect to class CustomerBooking
    //If I want to solve MovementRequirement instead of CancellationRequirement
    //Many things else
    // -----> These will affect to class CustomerBooking because too many dependencies
    //Solutions: use Interface to change objects that we use//Solutions: You create "interface" to change typeData you want and it wil not affect to class CustomerBooking
    // -----> I will demo in package named: LooseCoupling

    CustomerBooking(){}
    void reservationCheckout(int IDCustomer, String roomName, String email){
        CustomerInformation customerInformation = new CustomerInformation();
        customerInformation.getInfoCustomer(IDCustomer,roomName,email);
        EmailSender confirmation = new EmailSender();
        confirmation.sendConfirm(customerInformation.email);
        CancellationRequirement cancellation = new CancellationRequirement();
        cancellation.cancelRequest(customerInformation.ID,customerInformation.nameRoom);
    }
}
