package Coupling.TightCoupling;

public class TightCoupling {
    public static void main(String[] args) {
        //This Webapp will get info that customer enter and use method reservationCheckout of class CustomerBooking to solve
        int IDCustomer = 5;
        String roomName = "Single Room";
        String email = "nghiemxuanhieu97@gmail.com";
        CustomerBooking customerBooking = new CustomerBooking();
        customerBooking.reservationCheckout(IDCustomer,roomName,email);
    }

}

