package Coupling.TightCoupling;

public class CancellationRequirement {
    //This class has module: solve request cancellation of Customer
    void cancelRequest(int idCustomer, String nameRoom){
        System.out.println("Customer with ID: "+idCustomer+" cancelled "+nameRoom);
    }
}
