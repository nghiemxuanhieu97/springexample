package Coupling.TightCoupling;

public class CustomerInformation {
    int ID;
    String nameRoom;
    String email;

    //This class has module: get information of customer
    void getInfoCustomer(int ID, String nameRoom, String email){
        this.ID = ID;
        this.nameRoom = nameRoom;
        this.email = email;
        System.out.println("Info: ID = "+ID+", nameRoom = "+nameRoom+", mailBox = "+email);
    }
}
